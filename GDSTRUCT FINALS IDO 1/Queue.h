#pragma once
#include <iostream>
#include "UnorderedArray.h"

using namespace std;

template <class T>
class Queue
{
public:
	Queue(int size) : mArray(NULL)
	{
		mArray = new UnorderedArray<T>(size);
	}

	virtual void push(T Value)
	{
		mArray->push(Value);
	}
	virtual void pop()
	{
		for (int i = 0; i < mArray->getSize() - 1; i++)
		{
			mArray[0][i] = mArray[0][i + 1];
		}
		mArray->pop();
	}
	virtual const T& operator[](int index)
	{
		return mArray[0][index];
	}
	virtual int getSize()
	{
		return mArray->getSize();
	}
	virtual const T& top()
	{
		return mArray[0][0];
	}
	virtual void printQueueElements()
	{
		mArray->printElements();
		mArray->remove();
	}
private:
	UnorderedArray <T>* mArray;
};
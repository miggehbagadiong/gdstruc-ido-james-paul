#pragma once
#include <iostream>
#include "UnorderedArray.h"

using namespace std;

template <class T>
class Stack
{
public:
	Stack(int size) : mArray(NULL)
	{
		mArray = new UnorderedArray<T>(size);
	}

	virtual void push(T value)
	{
		mArray->push(value);
	}

	virtual void pop()
	{
		mArray->pop();
	}

	virtual const T& operator[](int index)
	{
		return mArray[0][index];
	}

	virtual int getSize()
	{
		return mArray->getSize();
	}

	virtual const T& top()
	{
		return mArray[0][mArray->getSize() - 1];
	}

	virtual void printStackElements()
	{
		/*for (int i = 0; i < mArray; i++)
		{
			cout << mArray[i] << endl;
		}*/

		mArray->printElements();
		mArray->remove();

	}
private:
	UnorderedArray<T>* mArray;
};


